var project = new Project('Breakdown');

project.addSources('Sources');
project.addDefine('kha_html5_disable_automatic_size_adjust');
project.targetOptions.html5.canvasId = 'breakdownkhanvas';
project.targetOptions.html5.scriptName = 'breakdown';

const android = project.targetOptions.android_native;
android.package = 'fi.lassepouru.breakdown';
android.versionCode = 1;
android.versionName = '1.0';
android.screenOrientation = 'portrait';
android.installLocation = "internalOnly";
android.metadata = [];
android.disableStickyImmersiveMode = true;
android.globalBuildGradlePath = 'data/android/build.gradle';
android.buildGradlePath = 'data/android/app/build.gradle';
android.proguardRulesPath = 'data/android/app/proguard-rules.pro';
android.customFilesPath = 'data/android/files';

resolve(project);
