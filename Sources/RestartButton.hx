/*
  Breakdown
  Copyright (C) 2023 Lasse Pouru

  This file is part of Breakdown.

  Breakdown is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

  Breakdown is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with Breakdown. If not, see <https://www.gnu.org/licenses/>.
*/

package;

import kha.graphics2.Graphics;
import kha.Color;
import kha.System;

class RestartButton {
  public var UNIT:Float;
  public var startTime:Float;
  public var pressed:Bool;

  public function new(unit: Float){
    this.UNIT = unit;
    this.startTime = 0;
    this.pressed = false;
  }

  public function render(graphics:Graphics){
    // arrow border
    graphics.color = Color.Black;
    graphics.fillRect(21 * UNIT, 30 * UNIT, 3 * UNIT, UNIT);
    graphics.fillRect(20 * UNIT, 31 * UNIT, 2 * UNIT, UNIT);
    graphics.fillRect(23 * UNIT, 31 * UNIT, 1 * UNIT, UNIT);
    graphics.fillRect(19 * UNIT, 32 * UNIT, 2 * UNIT, UNIT);
    graphics.fillRect(23 * UNIT, 32 * UNIT, 1 * UNIT, UNIT);
    graphics.fillRect(18 * UNIT, 33 * UNIT, 2 * UNIT, UNIT);
    graphics.fillRect(23 * UNIT, 33 * UNIT, 4 * UNIT, UNIT);
    graphics.fillRect(17 * UNIT, 34 * UNIT, 2 * UNIT, UNIT);
    graphics.fillRect(26 * UNIT, 34 * UNIT, 2 * UNIT, UNIT);
    graphics.fillRect(17 * UNIT, 35 * UNIT, 1 * UNIT, UNIT);
    graphics.fillRect(27 * UNIT, 35 * UNIT, 2 * UNIT, UNIT);
    graphics.fillRect(17 * UNIT, 36 * UNIT, 2 * UNIT, UNIT);
    graphics.fillRect(28 * UNIT, 36 * UNIT, 2 * UNIT, UNIT);
    graphics.fillRect(18 * UNIT, 37 * UNIT, 2 * UNIT, UNIT);
    graphics.fillRect(23 * UNIT, 37 * UNIT, 2 * UNIT, UNIT);
    graphics.fillRect(29 * UNIT, 37 * UNIT, 2 * UNIT, UNIT);
    graphics.fillRect(19 * UNIT, 38 * UNIT, 2 * UNIT, UNIT);
    graphics.fillRect(23 * UNIT, 38 * UNIT, 3 * UNIT, UNIT);
    graphics.fillRect(30 * UNIT, 38 * UNIT, 1 * UNIT, UNIT);
    graphics.fillRect(20 * UNIT, 39 * UNIT, 2 * UNIT, UNIT);
    graphics.fillRect(23 * UNIT, 39 * UNIT, 1 * UNIT, UNIT);
    graphics.fillRect(25 * UNIT, 39 * UNIT, 2 * UNIT, UNIT);
    graphics.fillRect(30 * UNIT, 39 * UNIT, 1 * UNIT, UNIT);
    graphics.fillRect(14 * UNIT, 40 * UNIT, 5 * UNIT, UNIT);
    graphics.fillRect(21 * UNIT, 40 * UNIT, 3 * UNIT, UNIT);
    graphics.fillRect(26 * UNIT, 40 * UNIT, 1 * UNIT, UNIT);
    graphics.fillRect(30 * UNIT, 40 * UNIT, 1 * UNIT, UNIT);
    graphics.fillRect(14 * UNIT, 41 * UNIT, 1 * UNIT, UNIT);
    graphics.fillRect(18 * UNIT, 41 * UNIT, 1 * UNIT, UNIT);
    graphics.fillRect(26 * UNIT, 41 * UNIT, 1 * UNIT, UNIT);
    graphics.fillRect(30 * UNIT, 41 * UNIT, 1 * UNIT, UNIT);
    graphics.fillRect(14 * UNIT, 42 * UNIT, 1 * UNIT, UNIT);
    graphics.fillRect(18 * UNIT, 42 * UNIT, 1 * UNIT, UNIT);
    graphics.fillRect(26 * UNIT, 42 * UNIT, 1 * UNIT, UNIT);
    graphics.fillRect(30 * UNIT, 42 * UNIT, 1 * UNIT, UNIT);
    graphics.fillRect(14 * UNIT, 43 * UNIT, 1 * UNIT, UNIT);
    graphics.fillRect(18 * UNIT, 43 * UNIT, 2 * UNIT, UNIT);
    graphics.fillRect(25 * UNIT, 43 * UNIT, 2 * UNIT, UNIT);
    graphics.fillRect(30 * UNIT, 43 * UNIT, 1 * UNIT, UNIT);
    graphics.fillRect(14 * UNIT, 44 * UNIT, 1 * UNIT, UNIT);
    graphics.fillRect(19 * UNIT, 44 * UNIT, 2 * UNIT, UNIT);
    graphics.fillRect(24 * UNIT, 44 * UNIT, 2 * UNIT, UNIT);
    graphics.fillRect(30 * UNIT, 44 * UNIT, 1 * UNIT, UNIT);
    graphics.fillRect(14 * UNIT, 45 * UNIT, 2 * UNIT, UNIT);
    graphics.fillRect(20 * UNIT, 45 * UNIT, 5 * UNIT, UNIT);
    graphics.fillRect(29 * UNIT, 45 * UNIT, 2 * UNIT, UNIT);
    graphics.fillRect(15 * UNIT, 46 * UNIT, 2 * UNIT, UNIT);
    graphics.fillRect(28 * UNIT, 46 * UNIT, 2 * UNIT, UNIT);
    graphics.fillRect(16 * UNIT, 47 * UNIT, 2 * UNIT, UNIT);
    graphics.fillRect(27 * UNIT, 47 * UNIT, 2 * UNIT, UNIT);
    graphics.fillRect(17 * UNIT, 48 * UNIT, 2 * UNIT, UNIT);
    graphics.fillRect(26 * UNIT, 48 * UNIT, 2 * UNIT, UNIT);
    graphics.fillRect(18 * UNIT, 49 * UNIT, 9 * UNIT, UNIT);

    // arrow
    graphics.color = Color.White;
    graphics.fillRect(22 * UNIT, 31 * UNIT, 1 * UNIT, UNIT);
    graphics.fillRect(21 * UNIT, 32 * UNIT, 2 * UNIT, UNIT);
    graphics.fillRect(20 * UNIT, 33 * UNIT, 3 * UNIT, UNIT);
    graphics.fillRect(19 * UNIT, 34 * UNIT, 7 * UNIT, UNIT);
    graphics.fillRect(18 * UNIT, 35 * UNIT, 9 * UNIT, UNIT);
    graphics.fillRect(19 * UNIT, 36 * UNIT, 9 * UNIT, UNIT);
    graphics.fillRect(20 * UNIT, 37 * UNIT, 3 * UNIT, UNIT);
    graphics.fillRect(25 * UNIT, 37 * UNIT, 4 * UNIT, UNIT);
    graphics.fillRect(21 * UNIT, 38 * UNIT, 2 * UNIT, UNIT);
    graphics.fillRect(26 * UNIT, 38 * UNIT, 4 * UNIT, UNIT);
    graphics.fillRect(22 * UNIT, 39 * UNIT, 1 * UNIT, UNIT);
    graphics.fillRect(27 * UNIT, 39 * UNIT, 3 * UNIT, UNIT);
    graphics.fillRect(27 * UNIT, 40 * UNIT, 3 * UNIT, UNIT);
    graphics.fillRect(15 * UNIT, 41 * UNIT, 3 * UNIT, UNIT);
    graphics.fillRect(27 * UNIT, 41 * UNIT, 3 * UNIT, UNIT);
    graphics.fillRect(15 * UNIT, 42 * UNIT, 3 * UNIT, UNIT);
    graphics.fillRect(27 * UNIT, 42 * UNIT, 3 * UNIT, UNIT);
    graphics.fillRect(15 * UNIT, 43 * UNIT, 3 * UNIT, UNIT);
    graphics.fillRect(27 * UNIT, 43 * UNIT, 3 * UNIT, UNIT);
    graphics.fillRect(15 * UNIT, 44 * UNIT, 4 * UNIT, UNIT);
    graphics.fillRect(26 * UNIT, 44 * UNIT, 4 * UNIT, UNIT);
    graphics.fillRect(16 * UNIT, 45 * UNIT, 4 * UNIT, UNIT);
    graphics.fillRect(25 * UNIT, 45 * UNIT, 4 * UNIT, UNIT);
    graphics.fillRect(17 * UNIT, 46 * UNIT, 11 * UNIT, UNIT);
    graphics.fillRect(18 * UNIT, 47 * UNIT, 9 * UNIT, UNIT);
    graphics.fillRect(19 * UNIT, 48 * UNIT, 7 * UNIT, UNIT);
    if (pressed) {
      var timer:Float = System.time - startTime;
      graphics.color = Color.Black;
      graphics.fillRect(24 * UNIT, 39 * UNIT, 1 * UNIT, UNIT);
      graphics.fillRect(18 * UNIT, 40 * UNIT, 2 * UNIT, UNIT);
      graphics.fillRect(25 * UNIT, 40 * UNIT, UNIT, 3 * UNIT);
      graphics.fillRect(19 * UNIT, 41 * UNIT, UNIT, 2 * UNIT);
      graphics.fillRect(20 * UNIT, 42 * UNIT, UNIT, UNIT);
      graphics.fillRect(24 * UNIT, 42 * UNIT, UNIT, UNIT);
      graphics.fillRect(21 * UNIT, 42 * UNIT, 3 * UNIT, UNIT);
      graphics.color = Color.White;
      graphics.fillRect(23 * UNIT, 37 * UNIT, 2 * UNIT, UNIT);
      graphics.fillRect(25 * UNIT, 38 * UNIT, 2 * UNIT, UNIT);
      graphics.fillRect(26 * UNIT, 39 * UNIT, 1 * UNIT, 5 * UNIT);
      graphics.fillRect(18 * UNIT, 41 * UNIT, 1 * UNIT, 3 * UNIT);
      graphics.fillRect(19 * UNIT, 44 * UNIT, 1 * UNIT, UNIT);
      graphics.fillRect(25 * UNIT, 44 * UNIT, 1 * UNIT, UNIT);
      graphics.fillRect(20 * UNIT, 45 * UNIT, 5 * UNIT, UNIT);
      if (timer >= 0.4) {
	graphics.color = Color.Black;
	graphics.fillRect(20 * UNIT, 40 * UNIT, UNIT, 3 * UNIT);
	graphics.fillRect(24 * UNIT, 40 * UNIT, UNIT, 3 * UNIT);
	graphics.fillRect(21 * UNIT, 42 * UNIT, UNIT, UNIT);
	graphics.fillRect(23 * UNIT, 42 * UNIT, UNIT, UNIT);
	graphics.fillRect(21 * UNIT, 43 * UNIT, 3 * UNIT, UNIT);
	graphics.color = Color.White;
	graphics.fillRect(23 * UNIT, 38 * UNIT, 2 * UNIT, UNIT);
	graphics.fillRect(24 * UNIT, 39 * UNIT, 2 * UNIT, UNIT);
	graphics.fillRect(25 * UNIT, 40 * UNIT, UNIT, 3 * UNIT);
	graphics.fillRect(19 * UNIT, 41 * UNIT, UNIT, 2 * UNIT);
	graphics.fillRect(19 * UNIT, 43 * UNIT, 2 * UNIT, UNIT);
	graphics.fillRect(24 * UNIT, 43 * UNIT, 2 * UNIT, UNIT);
	graphics.fillRect(20 * UNIT, 44 * UNIT, 5 * UNIT, UNIT);
      }
      if (timer >= 0.8) {
	graphics.color = Color.Black;
	graphics.fillRect(21 * UNIT, 41 * UNIT, 3 * UNIT, UNIT);
	graphics.fillRect(22 * UNIT, 42 * UNIT, UNIT, UNIT);
	graphics.color = Color.White;
	graphics.fillRect(23 * UNIT, 39 * UNIT, UNIT, 2 * UNIT);
	graphics.fillRect(24 * UNIT, 40 * UNIT, UNIT, 3 * UNIT);
	graphics.fillRect(20 * UNIT, 41 * UNIT, UNIT, 2 * UNIT);
	graphics.fillRect(21 * UNIT, 42 * UNIT, UNIT, UNIT);
	graphics.fillRect(23 * UNIT, 42 * UNIT, UNIT, UNIT);
	graphics.fillRect(21 * UNIT, 43 * UNIT, 3 * UNIT, UNIT);
      }
      if (timer >= 1.2) {
	graphics.fillRect(21 * UNIT, 41 * UNIT, UNIT, UNIT);
	graphics.fillRect(23 * UNIT, 41 * UNIT, UNIT, UNIT);
	graphics.fillRect(22 * UNIT, 42 * UNIT, UNIT, UNIT);
      }
    }
  }
}