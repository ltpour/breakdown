/*
  Breakdown
  Copyright (C) 2023 Lasse Pouru

  This file is part of Breakdown.

  Breakdown is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

  Breakdown is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with Breakdown. If not, see <https://www.gnu.org/licenses/>.
*/

package;

import kha.graphics2.Graphics;

class Block {
  public var x:Float;
  public var y:Float;
  public var width:Float;
  public var height:Float;
  public var acceleration:Float;
  public var yVel:Float;
  public var falling:Bool;
  public var UNIT:Float;

  public function new(x: Float, y: Float, width: Float, height: Float, acceleration: Float, unit: Float) {
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
    this.acceleration = acceleration;
    this.yVel = 0;
    this.falling = false;
    this.UNIT = unit;
  }

  public function move() {
    if (falling) {
      yVel += acceleration;
      y += yVel;
    }
  }

  public function render(graphics:Graphics) {
    graphics.fillRect(Math.fround(x / UNIT) * UNIT,            Math.fround(y / UNIT) * UNIT,            width, UNIT);
    graphics.fillRect(Math.fround(x / UNIT) * UNIT,            Math.fround(y / UNIT) * UNIT,            UNIT,     height);
    graphics.fillRect(Math.fround(x / UNIT) * UNIT + 3 * UNIT, Math.fround(y / UNIT) * UNIT,            UNIT,     height);
    graphics.fillRect(Math.fround(x / UNIT) * UNIT,            Math.fround(y / UNIT) * UNIT + 3 * UNIT, width, UNIT);
  }
}