/*
  Breakdown
  Copyright (C) 2023 Lasse Pouru

  This file is part of Breakdown.

  Breakdown is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

  Breakdown is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with Breakdown. If not, see <https://www.gnu.org/licenses/>.
*/

package;

import kha.graphics2.Graphics;

class Player {
  public var width:Float;
  public var height:Float;
  public var x:Float;
  public var y:Float;
  public var target:Float;
  public var speed:Float;
  public var targetReached:Bool;

  public function new(x: Float, y: Float, width: Float, height: Float, speed: Float){
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
    this.speed = speed;
    target = x;
    targetReached = false;
  }

  public function move(){
    if (target > x) {
      if (x + speed > target) {
	x = target;
	if (!targetReached) {
	  targetReached = true;
	}
      } else {
	x += speed;
	targetReached = false;
      }
    } else {
      if (x - speed < target) {
	x = target;
	if (!targetReached) {
	  targetReached = true;
	}
      } else {
	x -= speed;
	targetReached = false;
      }
    }
  }

  public function render(graphics:Graphics){
    graphics.fillRect(Math.fround(x / height) * height, Math.fround(y / height) * height, width, height);
  }
}