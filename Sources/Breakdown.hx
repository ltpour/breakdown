/*
  Breakdown
  Copyright (C) 2023 Lasse Pouru

  This file is part of Breakdown.

  Breakdown is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

  Breakdown is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with Breakdown. If not, see <https://www.gnu.org/licenses/>.
*/

package;

import haxe.ds.Vector;
import kha.Framebuffer;
import kha.Color;
import kha.Image;
import kha.Scaler;
import kha.ScreenCanvas;
import kha.Sound;
import kha.System;
import kha.audio2.Audio;
import kha.audio2.AudioChannel;
import kha.audio2.Buffer;
import kha.audio2.ResamplingAudioChannel;
import kha.audio2.StreamChannel;
#if kha_android_native
import kha.input.Surface;
#else
import kha.input.Mouse;
#end
#if cpp
import sys.thread.Mutex;
#end

enum State {
  Start;
  Game;
  LevelEnd;
  GameOver;
}

@:structInit class TimedTarget {
  public var time:Float;
  public var target:Float;
}

class Breakdown {
  public static inline var UNIT = 8;
  public static inline var DIGIT_WIDTH = 4;
  public static inline var DIGIT_HEIGHT = 5;
  public static var backbuffer:Image;

  var state:EnumValue;
  public var ball:Ball;
  public var scoreboard:Scoreboard;
  public static var player:Player;
  public var blocks:Array<Block>;
  public var restartButton:RestartButton;
  public var levelStartTime:Float;
#if kha_android_native
  public var latestTargets = new Map<Int, TimedTarget>();
#else
  public var mouseDown:Bool;
#end
#if cpp
  static var mutex: Mutex;
#end
  static inline var channelCount: Int = 32;
  static var soundChannels: Vector<AudioChannel>;
  static var streamChannels: Vector<StreamChannel>;
  static var internalSoundChannels: Vector<AudioChannel>;
  static var internalStreamChannels: Vector<StreamChannel>;
  static var sampleCache1: kha.arrays.Float32Array;
  static var sampleCache2: kha.arrays.Float32Array;
  static var lastAllocationCount: Int = 0;
  var collisionSound:Sound;
  var collisionLesserSound:Sound;
  var collisionBadSound:Sound;

  public function new(width:Int, height:Int) {
    initialize(width, height);
#if cpp
    mutex = new Mutex();
#end
    soundChannels = new Vector<AudioChannel>(channelCount);
    streamChannels = new Vector<StreamChannel>(channelCount);
    internalSoundChannels = new Vector<AudioChannel>(channelCount);
    internalStreamChannels = new Vector<StreamChannel>(channelCount);
    sampleCache1 = new kha.arrays.Float32Array(512);
    sampleCache2 = new kha.arrays.Float32Array(512);
    lastAllocationCount = 0;
    collisionSound = new Sound();
    collisionLesserSound = new Sound();
    collisionBadSound = new Sound();

    var stereo = 2;
    var frequencyMultiplier = 32;
    var volume = 0.25;
    var sampleCount = 4096;
    collisionSound.sampleRate = Audio.samplesPerSecond;
    collisionLesserSound.sampleRate = Audio.samplesPerSecond;
    collisionBadSound.sampleRate = Audio.samplesPerSecond;
    collisionSound.uncompressedData = new kha.arrays.Float32Array(sampleCount * 2);
    collisionLesserSound.uncompressedData = new kha.arrays.Float32Array(sampleCount * 2);
    collisionBadSound.uncompressedData = new kha.arrays.Float32Array(sampleCount * 2);
    for (i in 0 ... sampleCount) {
      for (j in 0 ... stereo) {
	collisionSound.uncompressedData[i * 2 + j] = Math.max(Math.min(Math.sin(i / sampleCount * 2 * Math.PI * frequencyMultiplier), 1.0), -1.0) / (1.0 / volume);
	collisionLesserSound.uncompressedData[i * 2 + j] = Math.max(Math.min(Math.sin(i / sampleCount * 1.5 * Math.PI * frequencyMultiplier), 1.0), -1.0) / (2.0 / volume);
	collisionBadSound.uncompressedData[i * 2 + j] = Math.max(Math.min(Math.sin(i / sampleCount * Math.PI * frequencyMultiplier), 1.0), -1.0) / (1.0 / volume);
	//collisionSound.uncompressedData[i * 2 + j] = 0;
      }
    }


    Audio.audioCallback = mix;
    //Audio.audioCallback = loopSine;
#if kha_android_native
    Surface.get().notify(onTouchDown, onTouchUp, onTouchMove);
#else
    // Mouse.get().hideSystemCursor();
    Mouse.get().notify(onMouseDown, onMouseUp, onMouseMove);
#end
  }

  public function initialize(width:Int, height:Int): Void {
    state = Start;
    backbuffer = Image.createRenderTarget(width, height);
    scoreboard = new Scoreboard(backbuffer.width, DIGIT_WIDTH, DIGIT_HEIGHT, UNIT);

    // FOR DEBUGGING
    //scoreboard.level = 1;

    nextLevel(width, height);
    restartButton = new RestartButton(UNIT);
  }

  public function nextLevel(width:Int, height:Int): Void {
    levelStartTime = System.time;
    scoreboard.level += 1;
    scoreboard.levelStr = Std.string(scoreboard.level);
    ball = new Ball(width / 2 - UNIT / 2, height / 2, UNIT, UNIT, 3 + scoreboard.level / 10);
    player = new Player(width / 2 - 3.5 * UNIT, height - 8 * UNIT, 7 * UNIT, UNIT, 8 + 2 * scoreboard.level / 10);
    blocks = [for (i in 0...8) for (j in 0...6) new Block((3 + i * 5) * UNIT, (9 + j * 5) * UNIT, 4 * UNIT, 4 * UNIT, 0.06 + 0.001 * scoreboard.level, UNIT)];
  }

  public function increaseSpeed(): Void {
    ball.setVelocity(3 + scoreboard.level / 10 + (System.time - levelStartTime) / 60);
    player.speed = 8 + 2 * scoreboard.level / 10 + (System.time - levelStartTime) / 60;
    for (block in blocks) {
      block.acceleration = 0.06 + 0.001 * (scoreboard.level + (System.time - levelStartTime) / 60);
    }
  }

  public function mix(samplesBox: kha.internal.IntBox, buffer: Buffer): Void {
    var samples = samplesBox.value;
    if (sampleCache1.length < samples) {
      if (Audio.disableGcInteractions) {
	trace("Unexpected allocation request in audio thread.");
	for (i in 0...samples) {
	  buffer.data.set(buffer.writeLocation, 0);
	  buffer.writeLocation += 1;
	  if (buffer.writeLocation >= buffer.size) {
	    buffer.writeLocation = 0;
	  }
	}
	lastAllocationCount = 0;
	Audio.disableGcInteractions = false;
	return;
      }
      sampleCache1 = new kha.arrays.Float32Array(samples * 2);
      sampleCache2 = new kha.arrays.Float32Array(samples * 2);
      lastAllocationCount = 0;
    }
    else {
      if (lastAllocationCount > 100) {
	Audio.disableGcInteractions = true;
      }
      else {
	lastAllocationCount += 1;
      }
    }

    for (i in 0...samples) {
      sampleCache2[i] = 0;
    }

    #if cpp
    mutex.acquire();
    #end
    for (i in 0...channelCount) {
      internalSoundChannels[i] = soundChannels[i];
    }
    for (i in 0...channelCount) {
      internalStreamChannels[i] = streamChannels[i];
    }
    #if cpp
    mutex.release();
    #end

    for (channel in internalSoundChannels) {
      if (channel == null || channel.finished)
	continue;
      channel.nextSamples(sampleCache1, samples, buffer.samplesPerSecond);
      for (i in 0...samples) {
	sampleCache2[i] += sampleCache1[i] * channel.volume;
      }
    }
    for (channel in internalStreamChannels) {
      if (channel == null || channel.finished)
	continue;
      channel.nextSamples(sampleCache1, samples, buffer.samplesPerSecond);
      for (i in 0...samples) {
	sampleCache2[i] += sampleCache1[i] * channel.volume;
      }
    }

    for (i in 0...samples) {
      buffer.data.set(buffer.writeLocation, Math.max(Math.min(sampleCache2[i], 1.0), -1.0));
      buffer.writeLocation += 1;
      if (buffer.writeLocation >= buffer.size) {
	buffer.writeLocation = 0;
      }
    }
  }

  public function loopSine(samples:kha.internal.IntBox, buffer:Buffer){
    var stereo = 2;
    var frequencyMultiplier = 16;
    var volume = 0.25;
    for (i in 0 ... samples.value) { // 4096
      for (j in 0 ... stereo) {
	buffer.data.set(buffer.writeLocation + j, Math.max(Math.min(Math.sin(i / samples.value * 2 * Math.PI * frequencyMultiplier), 1), -1) / (1 / volume));
      }
      buffer.writeLocation += stereo;
      if (buffer.writeLocation >= buffer.size) {
	buffer.writeLocation = 0;
      }
    }
  }

  public function play(sound: Sound, loop: Bool = false): kha.audio1.AudioChannel {
    var channel: kha.audio2.AudioChannel = null;
    if (Audio.samplesPerSecond != sound.sampleRate) {
      channel = new ResamplingAudioChannel(loop, sound.sampleRate);
    }
    else {
      channel = new AudioChannel(loop);
    }
    channel.data = sound.uncompressedData;
    var foundChannel = false;

    #if cpp
    mutex.acquire();
    #end
    for (i in 0...channelCount) {
      if (soundChannels[i] == null || soundChannels[i].finished) {
	soundChannels[i] = channel;
	foundChannel = true;
	break;
      }
    }
    #if cpp
    mutex.release();
    #end

    return foundChannel ? channel : null;
  }


  public function update(): Void {
    if (state == Game) {
      player.move();
      ball.move();
      for (block in blocks) {
	block.move();
      }

      // check for collisions

      // ball and left wall
      if (ball.x <= 2 * UNIT) {
	ball.xVel = Math.abs(ball.xVel);
	increaseSpeed();
	play(collisionLesserSound);
      }

      // ball and right wall
      if (ball.x >= backbuffer.width - 3 * UNIT) {
	ball.xVel = -Math.abs(ball.xVel);
	increaseSpeed();
	play(collisionLesserSound);
      }

      // ball and upper wall
      if (ball.y <= 7 * UNIT) {
	ball.yVel = Math.abs(ball.yVel);
	increaseSpeed();
	play(collisionLesserSound);
      }

      if (ball.y > backbuffer.height) {
	// game over
	state = GameOver;
	play(collisionBadSound);
#if kha_android_native
	if (latestTargets.keys().hasNext()) {
	  restartButton.startTime = System.time;
	  restartButton.pressed = true;
	}
#else
	if(mouseDown) {
	  restartButton.startTime = System.time;
	  restartButton.pressed = true;
	}
#end
      }

      // ball and player
      if (ball.y >= player.y && ball.y < player.y + ball.width && ball.x >= player.x - ball.width && ball.x <= player.x + player.width) {
	var angle = Math.max(0.1, Math.min(Math.abs(player.x + player.width - ball.x) / (player.width + ball.width) * 0.8 + 0.1, 0.9)) * Math.PI; // [0.1 * PI, 0.9 * PI]
	ball.xVel = ball.speed * Math.cos(angle);
	ball.yVel = -ball.speed * Math.sin(angle);
	increaseSpeed();
	play(collisionSound);
      }

      // TODO: putoavan palikan kiihtyvyys otettava huomioon ylhäältä/alhaalta törmätessä!
      for (block in blocks) {
	// block colliding with block
	for (block2 in blocks) {
	  if (block != block2 && Math.abs(block.x - block2.x) < block.width / 2) {
	    if (Math.abs(block.y - block2.y) < block.height / 2) {
	      if (block.y < block2.y) {
		block2.yVel = block.yVel;
		block.yVel = 0;
		block2.y = block.y + block.height;
		if (!block2.falling) {
		  scoreboard.score += 1;
		  scoreboard.scoreStr = Std.string(scoreboard.score);
		  block2.falling = true;
		}
	      } else if (block.y > block2.y) {
		block.yVel = block2.yVel;
		block2.yVel = 0;
		block.y = block2.y + block.height;
		if (!block.falling) {
		  scoreboard.score += 1;
		  scoreboard.scoreStr = Std.string(scoreboard.score);
		  block.falling = true;
		}
	      }
	    }
	  }
	}

	if (player.x + UNIT < block.x + block.width && player.x - UNIT > block.x - player.width && player.y < block.y + block.height && player.y > block.y - player.height) {
	  // collision with player, game over
	  state = GameOver;
	  play(collisionBadSound);
#if kha_android_native
	  if (latestTargets.keys().hasNext()) {
	    restartButton.startTime = System.time;
	    restartButton.pressed = true;
	  }
#else
	  if(mouseDown) {
	    restartButton.startTime = System.time;
	    restartButton.pressed = true;
	  }
#end
	}
	if (ball.x < block.x + block.width) {
	  // pallo ei palikan oikealla puolella erillään
	  if (ball.x > block.x - ball.width) {
	    // pallo ei palikan vasemmalla puolella erillään
	    if (ball.y < block.y + block.height) {
	      // pallo ei palikan alapuolella erillään
	      if (ball.y > block.y - ball.height) {
		// pallo ei palikan yläpuolella erillään
		increaseSpeed();
		if (!block.falling) {
		  block.falling = true; // törmäys
		  scoreboard.score += 1;
		  scoreboard.scoreStr = Std.string(scoreboard.score);
		  play(collisionSound);
		} else {
		  play(collisionLesserSound);
		}
		if (ball.x + ball.width / 2 < block.x + block.width / 2) {
		  // törmäys, pallo enemmän vasemmalla
		  if (ball.y + ball.height / 2 < block.y + block.height / 2) {
		    // törmäys, pallo enemmän vasemmalla ja ylhäällä
		    if (Math.abs(ball.x + ball.width - block.x) < Math.abs(ball.y + ball.height - block.y)) {
		      // törmäys, pallo eniten vasemmalla
		      ball.xVel = -Math.abs(ball.xVel);
		    } else {
		      // törmäys, pallo eniten ylhäällä
		      ball.yVel = -Math.abs(ball.yVel);
		    }
		  } else {
		    // törmäys, pallo enemmän vasemmalla ja alhaalla
		    if (Math.abs(ball.x + ball.width - block.x) < Math.abs(ball.y - (block.y + block.height))) {
		      // törmäys, pallo eniten vasemmalla
		      ball.xVel = -Math.abs(ball.xVel);
		    } else {
		      // törmäys, pallo eniten alhaalla
		      ball.yVel = Math.abs(ball.yVel);
		    }
		  }
		} else {
		  // törmäys, pallo enemmän oikealla
		  if (ball.y + ball.height / 2 < block.y + block.height / 2) {
		    // törmäys, pallo enemmän oikealla ja ylhäällä
		    if (Math.abs(ball.x - (block.x + block.width)) < Math.abs(ball.y + ball.height - block.y)) {
		      // törmäys, pallo eniten oikealla
		      ball.xVel = Math.abs(ball.xVel);
		    } else {
		      // törmäys, pallo eniten ylhäällä
		      ball.yVel = -Math.abs(ball.yVel);
		    }
		  } else {
		    // törmäys, pallo enemmän oikealla ja alhaalla
		    if (Math.abs(ball.x - (block.x + block.width)) < Math.abs(ball.y - (block.y + block.height))) {
		      // törmäys, pallo eniten oikealla
		      ball.xVel = Math.abs(ball.xVel);
		    } else {
		      // törmäys, pallo eniten alhaalla
		      ball.yVel = Math.abs(ball.yVel);
		    }
		  }
		}
	      }
	    }
	  }
	}
	if (block.y > backbuffer.height) {
	  // remove block
	  blocks.remove(block);
	}
      }
      if (blocks.length == 0 && state == Game) {
	state = LevelEnd;
      }
    } else if (state == GameOver && restartButton.pressed && System.time - restartButton.startTime >= 1.6) {
      initialize(backbuffer.width, backbuffer.height);
    }
  }

  public function render(framebuffer: Framebuffer): Void {
    var g = backbuffer.g2;
    g.begin();
    g.clear();
    g.color = Color.White;

    // borders
    g.fillRect(UNIT, (2 + DIGIT_HEIGHT) * UNIT, backbuffer.width - 2 * UNIT, UNIT);
    g.fillRect(UNIT, (2 + DIGIT_HEIGHT) * UNIT, UNIT, backbuffer.height - 2 * UNIT);
    g.fillRect(backbuffer.width - 2 * UNIT, (2 + DIGIT_HEIGHT) * UNIT, UNIT, backbuffer.height - 2 * UNIT);

    scoreboard.render(g);
    player.render(g);
    ball.render(g);
    for (b in blocks) {
      b.render(g);
    }
    if (state == GameOver) {
      restartButton.render(g);
    }
    g.end();

    framebuffer.g2.begin();
    Scaler.scale(backbuffer, framebuffer, System.screenRotation);
    framebuffer.g2.end();
  }

#if kha_android_native
  // touch controls
  function onTouchDown(id:Int, x:Int, y:Int){
    var target = Math.min(Math.max(Scaler.transformX(x, y, backbuffer, ScreenCanvas.the, System.screenRotation) - player.width / 2, 2 * UNIT), backbuffer.width - player.width - 2 * UNIT);
    var timedTarget:TimedTarget = {time: System.time, target: target};
    latestTargets.set(id, timedTarget);
    player.target = target;
    if (state == GameOver) {
      var keys = latestTargets.keys();
      keys.next();
      if (!keys.hasNext()) {
	restartButton.startTime = System.time;
	restartButton.pressed = true;
      }
    } else if (state == LevelEnd && blocks.length == 0) {
      nextLevel(backbuffer.width, backbuffer.height);
    }
  }

  function onTouchUp(id:Int, x:Int, y:Int){
    latestTargets.remove(id);
    if (state == Start) {
      state = Game;
    }
    if (state == Game) {
      var time = 0.0;
      var target = player.x;
      for (timedTarget in latestTargets) {
	if (timedTarget.time > time) {
	  target = timedTarget.target;
	}
      }
      player.target = target;
    } else if (state == LevelEnd && blocks.length > 0) {
      state = Game;
    } else if (state == GameOver) {
      if (!latestTargets.keys().hasNext()) {
	restartButton.pressed = false;
      }
    }
  }

  function onTouchMove(id:Int, x:Int, y:Int){
    var target = Math.min(Math.max(Scaler.transformX(x, y, backbuffer, ScreenCanvas.the, System.screenRotation) - player.width / 2, 2 * UNIT), backbuffer.width - player.width - 2 * UNIT);
    if (latestTargets.exists(id)) {
      latestTargets.get(id).time = System.time;
    } else {
      var timedTarget:TimedTarget = {time: System.time, target: target};
      latestTargets.set(id, timedTarget);
    }
    if (state == Game) {
      player.target = target;
    }
  }
#else
  // mouse controls
  public function onMouseDown(button:Int, x:Int, y:Int):Void {
    if (button == 0){
      mouseDown = true;
      player.target = Math.min(Math.max(Scaler.transformX(x, y, backbuffer, ScreenCanvas.the, System.screenRotation) - player.width / 2, 2 * UNIT), backbuffer.width - player.width - 2 * UNIT);
      if (state == GameOver) {
	restartButton.startTime = System.time;
	restartButton.pressed = true;
      } else if (state == LevelEnd && blocks.length == 0) {
	nextLevel(backbuffer.width, backbuffer.height);
      }
    }
  }

  public function onMouseUp(button:Int, x:Int, y:Int):Void {
    if (button == 0){
      mouseDown = false;
      if (state == Start) {
	state = Game;
      } else if (state == LevelEnd && blocks.length > 0) {
	state = Game;
      } else if (state == GameOver) {
	restartButton.pressed = false;
      }
    }
  }

  public function onMouseMove(x:Int, y:Int, cx:Int, cy:Int):Void {
    if (state == Game) {
      player.target = Math.min(Math.max(Scaler.transformX(x, y, backbuffer, ScreenCanvas.the, System.screenRotation) - player.width / 2, 2 * UNIT), backbuffer.width - player.width - 2 * UNIT);
    }
  }
#end
}
