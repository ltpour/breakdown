/*
  Breakdown
  Copyright (C) 2023 Lasse Pouru

  This file is part of Breakdown.

  Breakdown is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

  Breakdown is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with Breakdown. If not, see <https://www.gnu.org/licenses/>.
*/

package;

import kha.Assets;
import kha.Scheduler;
import kha.System;
#if kha_html5
import js.Browser.document;
import js.Browser.window;
import js.html.CanvasElement;
import kha.Macros;
#end

class Main {

  static inline final WIDTH:Int = 360;
  static inline final HEIGHT:Int = 640;

  public static function main() {
    setFullWindowCanvas();
    System.start({title: "Kha", width: WIDTH, height: HEIGHT}, function (_) {
	Assets.loadEverything(function () {
	    var breakdown = new Breakdown(WIDTH, HEIGHT);
	    Scheduler.addTimeTask(function () { breakdown.update(); }, 0, 1 / 60);
	    System.notifyOnFrames(function (framebuffers) { breakdown.render(framebuffers[0]); });
	  });
      });
  }

  static function setFullWindowCanvas():Void {
#if kha_html5
    document.documentElement.style.padding = "0";
    document.documentElement.style.margin = "0";
    document.body.style.padding = "0";
    document.body.style.margin = "0";
    final canvas:CanvasElement = cast document.getElementById(Macros.canvasId());
    canvas.style.display = "block";
    final resize = function() {
      var w = document.documentElement.clientWidth;
      var h = document.documentElement.clientHeight;
      if (w == 0 || h == 0) {
	w = window.innerWidth;
	h = window.innerHeight;
      }
      canvas.width = Std.int(w * window.devicePixelRatio);
      canvas.height = Std.int(h * window.devicePixelRatio);
      if (canvas.style.width == "") {
	canvas.style.width = "100%";
	canvas.style.height = "100%";
      }
    }
    window.onresize = resize;
    resize();
#end
  }
}
