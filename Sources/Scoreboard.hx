/*
  Breakdown
  Copyright (C) 2023 Lasse Pouru

  This file is part of Breakdown.

  Breakdown is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

  Breakdown is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with Breakdown. If not, see <https://www.gnu.org/licenses/>.
*/

package;

import kha.graphics2.Graphics;

class Scoreboard {
  public var DIGIT_WIDTH:Int;
  public var DIGIT_HEIGHT:Int;
  public var backbufferWidth:Int;
  public var UNIT:Int;
  public var level:Int;
  public var levelStr:String;
  public var score:Int;
  public var scoreStr:String;

  public function new(backbufferWidth: Int, digitWidth: Int, digitHeight: Int, unit: Int) {
    this.backbufferWidth = backbufferWidth;
    this.DIGIT_WIDTH = digitWidth;
    this.DIGIT_HEIGHT = digitHeight;
    this.UNIT = unit;
    this.level = 0;
    this.levelStr = Std.string(level);
    this.score = 0;
    this.scoreStr = Std.string(score);
  }

  public function update(){

  }

  public function render(g:Graphics) {
    // level number
    for (i in 0...levelStr.length) {
      switch (levelStr.charAt(i)) {
      case "0":
	g.fillRect(3 * UNIT + i * (DIGIT_WIDTH + 1) * UNIT, UNIT, DIGIT_WIDTH * UNIT, UNIT);
	g.fillRect(3 * UNIT + i * (DIGIT_WIDTH + 1) * UNIT, UNIT, UNIT, DIGIT_HEIGHT * UNIT);
	g.fillRect(3 * UNIT + i * (DIGIT_WIDTH + 1) * UNIT, DIGIT_HEIGHT * UNIT, DIGIT_WIDTH * UNIT, UNIT);
	g.fillRect((3 + DIGIT_WIDTH - 1) * UNIT + i * (DIGIT_WIDTH + 1) * UNIT, UNIT, UNIT, DIGIT_HEIGHT * UNIT);
      case "1":
	g.fillRect((3 + DIGIT_WIDTH - 1) * UNIT + i * (DIGIT_WIDTH + 1) * UNIT, UNIT, UNIT, DIGIT_HEIGHT * UNIT);
      case "2":
	g.fillRect(3 * UNIT + i * (DIGIT_WIDTH + 1) * UNIT, UNIT, DIGIT_WIDTH * UNIT, UNIT);
	g.fillRect((3 + DIGIT_WIDTH - 1) * UNIT + i * (DIGIT_WIDTH + 1) * UNIT, UNIT, UNIT, 3 * UNIT);
	g.fillRect(3 * UNIT + i * (DIGIT_WIDTH + 1) * UNIT, 3 * UNIT, DIGIT_WIDTH * UNIT, UNIT);
	g.fillRect(3 * UNIT + i * (DIGIT_WIDTH + 1) * UNIT, 3 * UNIT, UNIT, 3 * UNIT);
	g.fillRect(3 * UNIT + i * (DIGIT_WIDTH + 1) * UNIT, 5 * UNIT, DIGIT_WIDTH * UNIT, UNIT);
      case "3":
	g.fillRect(3 * UNIT + i * (DIGIT_WIDTH + 1) * UNIT, UNIT, DIGIT_WIDTH * UNIT, UNIT);
	g.fillRect((3 + DIGIT_WIDTH - 1) * UNIT + i * (DIGIT_WIDTH + 1) * UNIT, UNIT, UNIT, DIGIT_HEIGHT * UNIT);
	g.fillRect(3 * UNIT + i * (DIGIT_WIDTH + 1) * UNIT, 3 * UNIT, DIGIT_WIDTH * UNIT, UNIT);
	g.fillRect(3 * UNIT + i * (DIGIT_WIDTH + 1) * UNIT, 5 * UNIT, DIGIT_WIDTH * UNIT, UNIT);
      case "4":
	g.fillRect(3 * UNIT + i * (DIGIT_WIDTH + 1) * UNIT, UNIT, UNIT, 3 * UNIT);
	g.fillRect(3 * UNIT + i * (DIGIT_WIDTH + 1) * UNIT, 3 * UNIT, DIGIT_WIDTH * UNIT, UNIT);
	g.fillRect((3 + DIGIT_WIDTH - 1) * UNIT + i * (DIGIT_WIDTH + 1) * UNIT, UNIT, UNIT, DIGIT_HEIGHT * UNIT);
      case "5":
	g.fillRect(3 * UNIT + i * (DIGIT_WIDTH + 1) * UNIT, UNIT, DIGIT_WIDTH * UNIT, UNIT);
	g.fillRect(3 * UNIT + i * (DIGIT_WIDTH + 1) * UNIT, UNIT, UNIT, 3 * UNIT);
	g.fillRect(3 * UNIT + i * (DIGIT_WIDTH + 1) * UNIT, 3 * UNIT, DIGIT_WIDTH * UNIT, UNIT);
	g.fillRect((3 + DIGIT_WIDTH - 1) * UNIT + i * (DIGIT_WIDTH + 1) * UNIT, 3 * UNIT, UNIT, 3 * UNIT);
	g.fillRect(3 * UNIT + i * (DIGIT_WIDTH + 1) * UNIT, 5 * UNIT, DIGIT_WIDTH * UNIT, UNIT);
      case "6":
	g.fillRect(3 * UNIT + i * (DIGIT_WIDTH + 1) * UNIT, UNIT, DIGIT_WIDTH * UNIT, UNIT);
	g.fillRect(3 * UNIT + i * (DIGIT_WIDTH + 1) * UNIT, UNIT, UNIT, DIGIT_HEIGHT * UNIT);
	g.fillRect(3 * UNIT + i * (DIGIT_WIDTH + 1) * UNIT, 3 * UNIT, DIGIT_WIDTH * UNIT, UNIT);
	g.fillRect(3 * UNIT + i * (DIGIT_WIDTH + 1) * UNIT, DIGIT_HEIGHT * UNIT, DIGIT_WIDTH * UNIT, UNIT);
	g.fillRect((3 + DIGIT_WIDTH - 1) * UNIT + i * (DIGIT_WIDTH + 1) * UNIT, 3 * UNIT, UNIT, 3 * UNIT);
      case "7":
	g.fillRect(3 * UNIT + i * (DIGIT_WIDTH + 1) * UNIT, UNIT, DIGIT_WIDTH * UNIT, UNIT);
	g.fillRect((3 + DIGIT_WIDTH - 1) * UNIT + i * (DIGIT_WIDTH + 1) * UNIT, UNIT, UNIT, DIGIT_HEIGHT * UNIT);
      case "8":
	g.fillRect(3 * UNIT + i * (DIGIT_WIDTH + 1) * UNIT, UNIT, DIGIT_WIDTH * UNIT, UNIT);
	g.fillRect(3 * UNIT + i * (DIGIT_WIDTH + 1) * UNIT, UNIT, UNIT, DIGIT_HEIGHT * UNIT);
	g.fillRect(3 * UNIT + i * (DIGIT_WIDTH + 1) * UNIT, 3 * UNIT, DIGIT_WIDTH * UNIT, UNIT);
	g.fillRect(3 * UNIT + i * (DIGIT_WIDTH + 1) * UNIT, DIGIT_HEIGHT * UNIT, DIGIT_WIDTH * UNIT, UNIT);
	g.fillRect((3 + DIGIT_WIDTH - 1) * UNIT + i * (DIGIT_WIDTH + 1) * UNIT, UNIT, UNIT, DIGIT_HEIGHT * UNIT);
      case "9":
	g.fillRect(3 * UNIT + i * (DIGIT_WIDTH + 1) * UNIT, UNIT, DIGIT_WIDTH * UNIT, UNIT);
	g.fillRect(3 * UNIT + i * (DIGIT_WIDTH + 1) * UNIT, UNIT, UNIT, 3 * UNIT);
	g.fillRect(3 * UNIT + i * (DIGIT_WIDTH + 1) * UNIT, 3 * UNIT, DIGIT_WIDTH * UNIT, UNIT);
	g.fillRect(3 * UNIT + i * (DIGIT_WIDTH + 1) * UNIT, DIGIT_HEIGHT * UNIT, DIGIT_WIDTH * UNIT, UNIT);
	g.fillRect((3 + DIGIT_WIDTH - 1) * UNIT + i * (DIGIT_WIDTH + 1) * UNIT, UNIT, UNIT, DIGIT_HEIGHT * UNIT);
      case _:
      }
    }

    // score number
    for (i in 0...scoreStr.length) {
      switch (scoreStr.charAt(i)) {
      case "0":
	g.fillRect(backbufferWidth - 2 * UNIT - (scoreStr.length - i) * (DIGIT_WIDTH + 1) * UNIT, UNIT, DIGIT_WIDTH * UNIT, UNIT);
	g.fillRect(backbufferWidth - 2 * UNIT - (scoreStr.length - i) * (DIGIT_WIDTH + 1) * UNIT, UNIT, UNIT, DIGIT_HEIGHT * UNIT);
	g.fillRect(backbufferWidth - 2 * UNIT - (scoreStr.length - i) * (DIGIT_WIDTH + 1) * UNIT, DIGIT_HEIGHT * UNIT, DIGIT_WIDTH * UNIT, UNIT);
	g.fillRect(backbufferWidth - (2 - DIGIT_WIDTH + 1) * UNIT - (scoreStr.length - i) * (DIGIT_WIDTH + 1) * UNIT, UNIT, UNIT, DIGIT_HEIGHT * UNIT);
      case "1":
	g.fillRect(backbufferWidth - (2 - DIGIT_WIDTH + 1) * UNIT - (scoreStr.length - i) * (DIGIT_WIDTH + 1) * UNIT, UNIT, UNIT, DIGIT_HEIGHT * UNIT);
      case "2":
	g.fillRect(backbufferWidth - 2 * UNIT - (scoreStr.length - i) * (DIGIT_WIDTH + 1) * UNIT, UNIT, DIGIT_WIDTH * UNIT, UNIT);
	g.fillRect(backbufferWidth - (2 - DIGIT_WIDTH + 1) * UNIT - (scoreStr.length - i) * (DIGIT_WIDTH + 1) * UNIT, UNIT, UNIT, 3 * UNIT);
	g.fillRect(backbufferWidth - 2 * UNIT - (scoreStr.length - i) * (DIGIT_WIDTH + 1) * UNIT, 3 * UNIT, DIGIT_WIDTH * UNIT, UNIT);
	g.fillRect(backbufferWidth - 2 * UNIT - (scoreStr.length - i) * (DIGIT_WIDTH + 1) * UNIT, 3 * UNIT, UNIT, 3 * UNIT);
	g.fillRect(backbufferWidth - 2 * UNIT - (scoreStr.length - i) * (DIGIT_WIDTH + 1) * UNIT, 5 * UNIT, DIGIT_WIDTH * UNIT, UNIT);
      case "3":
	g.fillRect(backbufferWidth - 2 * UNIT - (scoreStr.length - i) * (DIGIT_WIDTH + 1) * UNIT, UNIT, DIGIT_WIDTH * UNIT, UNIT);
	g.fillRect(backbufferWidth - (2 - DIGIT_WIDTH + 1) * UNIT - (scoreStr.length - i) * (DIGIT_WIDTH + 1) * UNIT, UNIT, UNIT, DIGIT_HEIGHT * UNIT);
	g.fillRect(backbufferWidth - 2 * UNIT - (scoreStr.length - i) * (DIGIT_WIDTH + 1) * UNIT, 3 * UNIT, DIGIT_WIDTH * UNIT, UNIT);
	g.fillRect(backbufferWidth - 2 * UNIT - (scoreStr.length - i) * (DIGIT_WIDTH + 1) * UNIT, 5 * UNIT, DIGIT_WIDTH * UNIT, UNIT);
      case "4":
	g.fillRect(backbufferWidth - 2 * UNIT - (scoreStr.length - i) * (DIGIT_WIDTH + 1) * UNIT, UNIT, UNIT, 3 * UNIT);
	g.fillRect(backbufferWidth - 2 * UNIT - (scoreStr.length - i) * (DIGIT_WIDTH + 1) * UNIT, 3 * UNIT, DIGIT_WIDTH * UNIT, UNIT);
	g.fillRect(backbufferWidth - (2 - DIGIT_WIDTH + 1) * UNIT - (scoreStr.length - i) * (DIGIT_WIDTH + 1) * UNIT, UNIT, UNIT, DIGIT_HEIGHT * UNIT);
      case "5":
	g.fillRect(backbufferWidth - 2 * UNIT - (scoreStr.length - i) * (DIGIT_WIDTH + 1) * UNIT, UNIT, DIGIT_WIDTH * UNIT, UNIT);
	g.fillRect(backbufferWidth - 2 * UNIT - (scoreStr.length - i) * (DIGIT_WIDTH + 1) * UNIT, UNIT, UNIT, 3 * UNIT);
	g.fillRect(backbufferWidth - 2 * UNIT - (scoreStr.length - i) * (DIGIT_WIDTH + 1) * UNIT, 3 * UNIT, DIGIT_WIDTH * UNIT, UNIT);
	g.fillRect(backbufferWidth - (2 - DIGIT_WIDTH + 1) * UNIT - (scoreStr.length - i) * (DIGIT_WIDTH + 1) * UNIT, 3 * UNIT, UNIT, 3 * UNIT);
	g.fillRect(backbufferWidth - 2 * UNIT - (scoreStr.length - i) * (DIGIT_WIDTH + 1) * UNIT, 5 * UNIT, DIGIT_WIDTH * UNIT, UNIT);
      case "6":
	g.fillRect(backbufferWidth - 2 * UNIT - (scoreStr.length - i) * (DIGIT_WIDTH + 1) * UNIT, UNIT, DIGIT_WIDTH * UNIT, UNIT);
	g.fillRect(backbufferWidth - 2 * UNIT - (scoreStr.length - i) * (DIGIT_WIDTH + 1) * UNIT, UNIT, UNIT, DIGIT_HEIGHT * UNIT);
	g.fillRect(backbufferWidth - 2 * UNIT - (scoreStr.length - i) * (DIGIT_WIDTH + 1) * UNIT, 3 * UNIT, DIGIT_WIDTH * UNIT, UNIT);
	g.fillRect(backbufferWidth - 2 * UNIT - (scoreStr.length - i) * (DIGIT_WIDTH + 1) * UNIT, DIGIT_HEIGHT * UNIT, DIGIT_WIDTH * UNIT, UNIT);
	g.fillRect(backbufferWidth - (2 - DIGIT_WIDTH + 1) * UNIT - (scoreStr.length - i) * (DIGIT_WIDTH + 1) * UNIT, 3 * UNIT, UNIT, 3 * UNIT);
      case "7":
	g.fillRect(backbufferWidth - 2 * UNIT - (scoreStr.length - i) * (DIGIT_WIDTH + 1) * UNIT, UNIT, DIGIT_WIDTH * UNIT, UNIT);
	g.fillRect(backbufferWidth - (2 - DIGIT_WIDTH + 1) * UNIT - (scoreStr.length - i) * (DIGIT_WIDTH + 1) * UNIT, UNIT, UNIT, DIGIT_HEIGHT * UNIT);
      case "8":
	g.fillRect(backbufferWidth - 2 * UNIT - (scoreStr.length - i) * (DIGIT_WIDTH + 1) * UNIT, UNIT, DIGIT_WIDTH * UNIT, UNIT);
	g.fillRect(backbufferWidth - 2 * UNIT - (scoreStr.length - i) * (DIGIT_WIDTH + 1) * UNIT, UNIT, UNIT, DIGIT_HEIGHT * UNIT);
	g.fillRect(backbufferWidth - 2 * UNIT - (scoreStr.length - i) * (DIGIT_WIDTH + 1) * UNIT, 3 * UNIT, DIGIT_WIDTH * UNIT, UNIT);
	g.fillRect(backbufferWidth - 2 * UNIT - (scoreStr.length - i) * (DIGIT_WIDTH + 1) * UNIT, DIGIT_HEIGHT * UNIT, DIGIT_WIDTH * UNIT, UNIT);
	g.fillRect(backbufferWidth - (2 - DIGIT_WIDTH + 1) * UNIT - (scoreStr.length - i) * (DIGIT_WIDTH + 1) * UNIT, UNIT, UNIT, DIGIT_HEIGHT * UNIT);
      case "9":
	g.fillRect(backbufferWidth - 2 * UNIT - (scoreStr.length - i) * (DIGIT_WIDTH + 1) * UNIT, UNIT, DIGIT_WIDTH * UNIT, UNIT);
	g.fillRect(backbufferWidth - 2 * UNIT - (scoreStr.length - i) * (DIGIT_WIDTH + 1) * UNIT, UNIT, UNIT, 3 * UNIT);
	g.fillRect(backbufferWidth - 2 * UNIT - (scoreStr.length - i) * (DIGIT_WIDTH + 1) * UNIT, 3 * UNIT, DIGIT_WIDTH * UNIT, UNIT);
	g.fillRect(backbufferWidth - 2 * UNIT - (scoreStr.length - i) * (DIGIT_WIDTH + 1) * UNIT, DIGIT_HEIGHT * UNIT, DIGIT_WIDTH * UNIT, UNIT);
	g.fillRect(backbufferWidth - (2 - DIGIT_WIDTH + 1) * UNIT - (scoreStr.length - i) * (DIGIT_WIDTH + 1) * UNIT, UNIT, UNIT, DIGIT_HEIGHT * UNIT);
      case _:
      }
    }
  }
}