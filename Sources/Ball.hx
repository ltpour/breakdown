/*
  Breakdown
  Copyright (C) 2023 Lasse Pouru

  This file is part of Breakdown.

  Breakdown is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

  Breakdown is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with Breakdown. If not, see <https://www.gnu.org/licenses/>.
*/

package;

import kha.graphics2.Graphics;

class Ball {
  public var width:Float;
  public var height:Float;
  public var x:Float;
  public var y:Float;
  public var speed:Float;
  public var xVel:Float;
  public var yVel:Float;

  public function new(x: Float, y: Float, width: Float, height: Float, speed: Float) {
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
    this.speed = speed;
    var angle = (Math.random() / 2 + 0.25) * Math.PI;
    this.xVel = speed * Math.cos(angle);
    this.yVel = speed * Math.sin(angle);
  }

  public function setVelocity(speed: Float) {
    var angle = Math.atan2(yVel, xVel);
    xVel = speed * Math.cos(angle);
    yVel = speed * Math.sin(angle);
  }

  public function move() {
    x += xVel;
    y += yVel;
  }

  public function render(graphics:Graphics) {
    graphics.fillRect(Math.fround(x / height) * height, Math.fround(y / height) * height, width, height);
  }
}